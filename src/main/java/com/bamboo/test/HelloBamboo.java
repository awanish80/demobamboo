/**
 * 
 */
package com.bamboo.test;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Asingh
 *
 */
@RestController
public class HelloBamboo {
	@GetMapping("/hello")
	public String sayHelloBamboo() {
		return "Hello Bamboo setup";
		
	}

}
